export default function () {

  this.namespace = '/api';

  this.get('/invoices', function (db, request) {
    // return {
    //   data: db.invoices.map(attrs => (
    //     { type: 'invoice', id: attrs.id, attributes: attrs }
    //   ))
    // }

    let invoices = [{id: 1, amount: 50, date: '2019-05-01'},
                    {id: 2, amount: 150, date: '2019-05-02'},
                    {id: 3, amount: 200, date: '2019-05-03'},
                    {id: 4, amount: 250, date: '2019-05-04'},];

      if(request.queryParams.date !== undefined) {
        let filteredInvoices = invoices.filter(function(i) {
          return i.date.indexOf(request.queryParams.date.toLowerCase()) !== -1;
        });
        return { invoices: filteredInvoices };
      } else {
        return { invoices: invoices };
      }

  })

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `/api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    http://www.ember-cli-mirage.com/docs/v0.4.x/shorthands/
  */
}
