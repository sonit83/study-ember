/* eslint-disable no-console */
import Component from '@ember/component';

export default Component.extend({
    actions: {
        popupEdit(id) {
            console.log(id);
            alert('Popup edit for record id = ' + id);
        },

        deleteRecord(id) {
            console.log(id);
            alert('Call delete for record id = ' + id);
        }
    }
});
