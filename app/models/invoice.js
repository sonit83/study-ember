import DS from 'ember-data';

export default DS.Model.extend({
    // id: DS.attr(),
    amount: DS.attr('number'),
    date: DS.attr('date')
  });